package com.isw.svcord19.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord19.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord19.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord19.sdk.domain.svcord.entity.Svcord19;
import com.isw.svcord19.sdk.domain.svcord.entity.Svcord19Entity;
import com.isw.svcord19.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord19.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord19.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord19.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord19.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord19.sdk.integration.partylife.entity.RetrieveLoginServiceInput;
import com.isw.svcord19.sdk.integration.partylife.entity.RetrieveLoginServiceOutput;
import com.isw.svcord19.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord19.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord19.domain.svcord.command.Svcord19Command;
import com.isw.svcord19.integration.partylife.service.RetrieveLoginService;
import com.isw.svcord19.integration.paymord.service.PaymentOrder;
import com.isw.svcord19.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord19.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord19Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLoginService retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;
  

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord19.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord19.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // User validation check
    RetrieveLoginServiceInput retrieveInput = integrationEntityBuilder
    .getPartylife()
    .getRetrieveLoginServiceInput()
    .setId("test1")
    .build();

    RetrieveLoginServiceOutput retriveSerivceOutput = retrieveLogin.execute(retrieveInput);
    
    if(retriveSerivceOutput.getResult() != "SUCCESS") {
      return null;
    }

    // Rootentity servicing order 생성
    CustomerReferenceEntity customerReference = this.entityBuilder
    .getSvcord()
    .getCustomerReference().build();

    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createInput = this.entityBuilder.getSvcord()
    .getCreateServicingOrderProducerInput().build();

    createInput.setCustomerReference(customerReference);
    Svcord19 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);


    //payment order호출
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD19");
    paymentOrderInput.setExternalSerive("SVCORD19");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    //pyment order 결과 updates
    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
    .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(createOutput.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord().getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();

    return withdrawalDomainServiceOutput;
  }

}
