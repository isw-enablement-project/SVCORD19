package com.isw.svcord19.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord19.sdk.domain.svcord.command.Svcord19CommandBase;
import com.isw.svcord19.sdk.domain.svcord.entity.Svcord19Entity;
import com.isw.svcord19.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord19.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord19.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord19.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord19.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord19.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord19Command extends Svcord19CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord19Command.class);

  public Svcord19Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord19.sdk.domain.svcord.entity.Svcord19 createServicingOrderProducer(com.isw.svcord19.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord19Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic
      Svcord19Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord19().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);
      
      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

      return repo.getSvcord().getSvcord19().save(servicingOrderProcedure);
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord19.sdk.domain.svcord.entity.Svcord19 instance, com.isw.svcord19.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord19Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic
     
      Svcord19Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord19().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

      this.repo.getSvcord().getSvcord19().save(servicingOrderProcedure);
    }
  
}
