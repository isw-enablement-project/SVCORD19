package com.isw.svcord19.api.v1;

import com.isw.svcord19.domain.svcord.service.WithdrawalDomainService;
import com.isw.svcord19.sdk.api.v1.api.WithdrawalApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import com.isw.svcord19.sdk.api.v1.model.ErrorSchema;
import com.isw.svcord19.sdk.api.v1.model.WithdrawalBodySchema;
import com.isw.svcord19.sdk.api.v1.model.WithdrawalResponseSchema;
import com.isw.svcord19.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord19.sdk.domain.svcord.entity.WithdrawalDomainServiceInput;
import com.isw.svcord19.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * A stub that provides implementation for the WithdrawalApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.isw.svcord19.sdk.api.v1.api")
public class WithdrawalApiV1Provider implements WithdrawalApiV1Delegate {

  @Autowired
  WithdrawalDomainService withdrawaDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;

  @Override
  public ResponseEntity<WithdrawalResponseSchema> servicingOrderWithdrawalPost(WithdrawalBodySchema withdrawalBodySchema) {
    //TODO Auto-generated method stub
    WithdrawalResponseSchema withdrawalResponse = new WithdrawalResponseSchema();
    
    WithdrawalDomainServiceInput withdrawalInput = entityBuilder.getSvcord()
    .getWithdrawalDomainServiceInput()
    .setAccountNumber(withdrawalBodySchema.getAccountNumber())
    .setAmount(withdrawalBodySchema.getAmount())
    .build();
    
    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = withdrawaDomainService.execute(withdrawalInput);

    
  if(withdrawalDomainServiceOutput == null){
    ErrorSchema errorSchema = new ErrorSchema();
    errorSchema.setErrorId("500");
    errorSchema.setErrorMsg("INTERNAL ERROR");
    ResponseEntity.status(500).body(errorSchema);
  }

  withdrawalResponse.setServicingOrderWorkTaskResult(withdrawalDomainServiceOutput.getServicingOrderWorkResult().toString());
  withdrawalResponse.setTransactionID(withdrawalDomainServiceOutput.getTrasactionId());

  return ResponseEntity.status(200).body(withdrawalResponse);
  }

}
