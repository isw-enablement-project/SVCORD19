package com.isw.svcord19.integration.paymord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.isw.svcord19.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord19.sdk.integration.paymord.paymord.model.CreatePaymentOrderRequest;
import com.isw.svcord19.sdk.integration.paymord.paymord.model.CreatePaymentOrderResponse;
import com.isw.svcord19.sdk.integration.paymord.paymord.model.CusotmerReference;
import com.isw.svcord19.sdk.integration.paymord.paymord.model.PaymentInitiatorSchema;
import com.isw.svcord19.sdk.integration.paymord.paymord.model.CreatePaymentOrderRequest.PaymentTypeEnum;
import com.isw.svcord19.sdk.integration.paymord.paymord.provider.PaymentOrderApiPaymord;
import com.isw.svcord19.sdk.integration.paymord.service.PaymentOrderBase;
import com.isw.svcord19.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class PaymentOrder extends PaymentOrderBase {

  private static final Logger log = LoggerFactory.getLogger(PaymentOrder.class);

  @Autowired
  PaymentOrderApiPaymord paymentOrderAPI;

  public PaymentOrder(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord19.sdk.integration.paymord.entity.PaymentOrderOutput execute(com.isw.svcord19.sdk.integration.paymord.entity.PaymentOrderInput paymentOrderInput)  {

    log.info("PaymentOrder.execute()");
    // TODO: Add your service implementation logic
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("accept", "application/json");
    httpHeaders.set("Content-Type", "application/json");
    
    CreatePaymentOrderRequest createPaymentOrderRequest = new CreatePaymentOrderRequest();
    CusotmerReference customerReference = new CusotmerReference();
    customerReference.setAccountNumber(paymentOrderInput.getAccountNumber());
    customerReference.setAmount(paymentOrderInput.getAmount());
    PaymentInitiatorSchema paymentInitiator = new PaymentInitiatorSchema();
    paymentInitiator.setExternalID(paymentOrderInput.getExternalId());
    paymentInitiator.setExternalService(paymentOrderInput.getExternalSerive());

    createPaymentOrderRequest.setCustomerReference(customerReference);
    createPaymentOrderRequest.setPaymentInitiator(paymentInitiator);
    createPaymentOrderRequest.setPaymentType(PaymentTypeEnum.CASH_WITHDRAWAL);

    CreatePaymentOrderResponse paymentOrderResponse = paymentOrderAPI.createPaymentOrder(createPaymentOrderRequest, httpHeaders).getBody();
   
    PaymentOrderOutput paymentOrderOutput = this.entityBuilder.getPaymord().getPaymentOrderOutput().build();
    paymentOrderOutput.setPaymentOrderReulst(paymentOrderResponse.getContents().getPaymentOrderResult());
    paymentOrderOutput.setTransactionId(paymentOrderOutput.getTransactionId());
    return paymentOrderOutput;
  }

}
