package com.isw.svcord19.integration.partylife.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.isw.svcord19.sdk.integration.partylife.entity.RetrieveLoginServiceOutput;
import com.isw.svcord19.sdk.integration.partylife.partylife.model.LoginResponse;
import com.isw.svcord19.sdk.integration.partylife.partylife.provider.LoginApiPartylife;
import com.isw.svcord19.sdk.integration.partylife.service.RetrieveLoginServiceBase;
import com.isw.svcord19.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class RetrieveLoginService extends RetrieveLoginServiceBase {

  private static final Logger log = LoggerFactory.getLogger(RetrieveLoginService.class);

  @Autowired
  LoginApiPartylife partyLife;


  public RetrieveLoginService(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord19.sdk.integration.partylife.entity.RetrieveLoginServiceOutput execute(com.isw.svcord19.sdk.integration.partylife.entity.RetrieveLoginServiceInput retrieveLoginServiceInput)  {

    log.info("RetrieveLoginService.execute()");
    // TODO: Add your service implementation logic

    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("accept", "application/json");
    httpHeaders.set("Content-Type", "application/json");  
 
    LoginResponse loginResponse = partyLife.getUserLogin(retrieveLoginServiceInput.getId(), httpHeaders).getBody();

    RetrieveLoginServiceOutput loginOuput = this.entityBuilder.getPartylife().getRetrieveLoginServiceOutput().build();
    loginOuput.setResult(loginResponse.getResult().getValue());

    return loginOuput;
  }

}
